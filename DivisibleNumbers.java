package algorithms;

public class DivisibleNumbers {

    void numbersDivisibleByThreeAndFive() {
        divisibleByThree();
        divisibleByFive();
        divisibleByThreeAndFive();
    }

    private void divisibleByThree() {
        System.out.println("Numbers divisible by 3");
        for (int i = 1; i < 101; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }
    }

    private void divisibleByFive() {
        System.out.println("Numbers divisible by 5");
        for (int i = 1; i < 101; i++) {
            if (i % 5 == 0) {
                System.out.println(i);
            }
        }
    }

    private void divisibleByThreeAndFive() {
        System.out.println("Numbers divisible by 3 and 5");
        for (int i = 1; i < 101; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println(i);
            }
        }
    }
}
