package algorithms;

public class HeatWater {
    private static final int SPECIFIC_HEAT_OF_WATER = 4200;
    private double amountOfWater;
    private double initialTemperature;
    private double finalTemperature;

    public HeatWater(double amountOfWater, double initialTemperature, double finalTemperature) {
        this.amountOfWater = amountOfWater;
        this.initialTemperature = initialTemperature;
        this.finalTemperature = finalTemperature;
    }

    double calculateEnergy() {

        return SPECIFIC_HEAT_OF_WATER * amountOfWater * (finalTemperature - initialTemperature);
    }
}
