package algorithms;

public class CountSpaces {

    void printNumberOfSpaces(String chars) {
        int count = 0;
        for (int i = 0; i < chars.length() ; i++) {
            if (chars.charAt(i) == ' ') count++;
        }
        System.out.println(count);
    }
}
