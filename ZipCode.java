package algorithms;

public class ZipCode {
    private String zipCode;
    private String longLine = "*****\n";

    public ZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    void printBarCode() {
        StringBuilder sb = new StringBuilder();
        char checksum = calculateChecksum();
        sb.append(longLine);
        for (int i = 0; i < 5; i++) {
            sb.append(checkCode(zipCode.charAt(i)));
        }
        sb.append(checkCode(checksum));
        sb.append(longLine);
        System.out.println(sb.toString());
    }

    private char calculateChecksum() {
        int sum = 0;
        int code = Integer.parseInt(zipCode);
        while (code > 0) {
            sum += code % 10;
            code /= 10;
        }
        return Character.forDigit(sum % 10, 10);
    }

    private String checkCode(char value) {
        StringBuilder sb = new StringBuilder();
        String shortLine = "**\n";
        switch (value) {
            case '0':
                sb.append(longLine).append(longLine).append(shortLine).append(shortLine).append(shortLine);
                break;
            case '1':
                sb.append(shortLine).append(shortLine).append(shortLine).append(longLine).append(longLine);
                break;
            case '2':
                sb.append(shortLine).append(shortLine).append(longLine).append(shortLine).append(longLine);
                break;
            case '3':
                sb.append(shortLine).append(shortLine).append(longLine).append(longLine).append(shortLine);
                break;
            case '4':
                sb.append(shortLine).append(longLine).append(shortLine).append(shortLine).append(longLine);
                break;
            case '5':
                sb.append(shortLine).append(longLine).append(shortLine).append(longLine).append(shortLine);
                break;
            case '6':
                sb.append(shortLine).append(longLine).append(longLine).append(shortLine).append(shortLine);
                break;
            case '7':
                sb.append(longLine).append(shortLine).append(shortLine).append(shortLine).append(longLine);
                break;
            case '8':
                sb.append(longLine).append(shortLine).append(shortLine).append(longLine).append(shortLine);
                break;
            default:
                sb.append(longLine).append(shortLine).append(longLine).append(shortLine).append(shortLine);

        }
        return sb.toString();
    }
}
