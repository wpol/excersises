package algorithms;

public class ChineseZodiac {
    private int year;

    public ChineseZodiac(int year) {
        this.year = year;
    }

    void displayChineseZodiacSign() {
        String zodiacSign;
        int remainder = Math.abs((year) - 1900) % 12;
        switch (remainder) {
            case 0:
                zodiacSign = "rat";
                break;
            case 1:
                zodiacSign = "ox";
                break;
            case 2:
                zodiacSign = "tiger";
                break;
            case 3:
                zodiacSign = "rabbit";
                break;
            case 4:
                zodiacSign = "dragon";
                break;
            case 5:
                zodiacSign = "snake";
                break;
            case 6:
                zodiacSign = "horse";
                break;
            case 7:
                zodiacSign = "sheep";
                break;
            case 8:
                zodiacSign = "monkey";
                break;
            case 9:
                zodiacSign = "rooster";
                break;
            case 10:
                zodiacSign = "dog";
                break;
            default:
                zodiacSign = "pig";
        }
        System.out.println("Your Chinese zodiac sign is " + zodiacSign);
    }
}
