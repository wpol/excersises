package algorithms;

public class HoursMinuteSecond {
    private int sec;

    public HoursMinuteSecond(int sec) {
        this.sec = sec;
    }

    StringBuilder sb = new StringBuilder();

    public void printHour() {
        sb.append(sec / 3600).append(":").append((sec / 60) % 60)
                .append(":").append((sec / 60) % 60);
        System.out.println(sb.toString());
    }
}
