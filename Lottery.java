package algorithms;

import java.util.Random;

public class Lottery {
    private int number;
    private int lotteryNumber;

    public Lottery(int number) {
        this.number = number;
    }

    void drawLots() {
        Random random = new Random();
        lotteryNumber = random.nextInt(100);
        int result = checkNumber();
        if (result == 0) {
            System.out.println("You loose!");
        } else {
            System.out.println("You won $" + result);
        }
    }

    private int checkNumber() {
        StringBuilder sb = new StringBuilder();
        String gamblerNumber;
        String lotsNumber;
        if (number < 10) {
            gamblerNumber = sb.append(0).append(number).toString();
            sb.delete(0, 10);
        } else gamblerNumber = String.valueOf(number);
        if (lotteryNumber < 10) {
            lotsNumber = sb.append(0).append(lotteryNumber).toString();
            sb.delete(0, 10);
        } else lotsNumber = String.valueOf(lotteryNumber);

        if (gamblerNumber.equals(lotsNumber)) return 10000;
        if (gamblerNumber.equals(sb.append(lotsNumber).reverse().toString())) return 3000;
        if (gamblerNumber.charAt(0) == lotsNumber.charAt(0) || gamblerNumber.charAt(0) == lotsNumber.charAt(1)
                || gamblerNumber.charAt(1) == lotsNumber.charAt(0) || gamblerNumber.charAt(1) == lotsNumber.charAt(1)) {
            return 1000;
        }

        return 0;
    }
}
