package algorithms;

public class TwoDimension {
    private int n;
    private int[][] array;

    public TwoDimension(int n) {
        this.n = n;
    }

    public void createAndPrintArray() {
        createArray();
        printArray();
    }

    private void createArray() {
        array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i; j++) {
                array[i][j] = 1 + i + j;
            }
            int k = i;
            for (int j = n - 1; j > n - i - 1; j--) {
                array[i][j] = k--;
            }
        }
    }

    private void printArray() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(array[i][j]);
            }
            System.out.println();
        }
    }
}
