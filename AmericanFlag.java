package algorithms;

public class AmericanFlag {
    private int length = 39;

    void printAmerianFlag() {
        printHighPart();
        printLowPart();
    }

    private void printHighPart() {
        for (int i = 0; i < 9; i++) {
            if (i % 2 == 0) {
                printEvenLine();
            } else printOddLine();
        }
    }

    private void printLowPart() {
        for (int i = 0; i < 6; i++) {
            printLongLine();
        }
    }

    private void printEvenLine() {
        printLongStars();
        printShortLine();
    }

    private void printOddLine() {
        printShortStars();
        printShortLine();
    }

    private void printLongStars() {
        for (int i = 0; i < 6; i++) {
            System.out.print("* ");
        }
    }

    private void printShortStars() {
        System.out.print(" ");
        for (int i = 0; i < 5; i++) {
            System.out.print("* ");
        }
        System.out.print(" ");
    }

    private void printShortLine() {
        for (int i = 0; i < length; i++) {
            System.out.print("=");
        }
        System.out.print("\n");
    }

    private void printLongLine() {
        for (int i = 0; i < length + 12; i++) {
            System.out.print("=");
        }
        System.out.print("\n");
    }


}
