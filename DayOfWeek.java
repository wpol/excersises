package algorithms;

public class DayOfWeek {
    private int year;
    private int month;
    private int dayOfMonth;

    public DayOfWeek(int year, int month, int dayOfMonth) {
        this.year = year;
        this.month = month;
        this.dayOfMonth = dayOfMonth;
    }

    void displayDayOfWeek() {
        String nameOfDay = calculateDay();
        System.out.println(nameOfDay);
    }

    private String calculateDay() {
        String nameOfDay;
        if (month == 1) {
            month = 13;
            year = year - 1;
        }
        if (month == 2) {
            month = 14;
            year = year - 1;
        }
        int h = (dayOfMonth + 26 * (month + 1) / 10 + (year % 100) + (year % 100) / 4 + (year / 100) / 4 + 5 * (year / 100)) % 7;
        switch (h) {
            case 0:
                nameOfDay = "Saturday";
                break;
            case 1:
                nameOfDay = "Sunday";
                break;
            case 2:
                nameOfDay = "Monday";
                break;
            case 3:
                nameOfDay = "Tuesday";
                break;
            case 4:
                nameOfDay = "Wednesday";
                break;
            case 5:
                nameOfDay = "Thursday";
                break;
            default:
                nameOfDay = "Saturday";
        }
        return nameOfDay;
    }
}
