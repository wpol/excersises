package algorithms;

public class SpirallingArray {
    private int[][] array;
    private int rowHigh;
    private int rowLow;
    private int colLeft;
    private int colRight;
    private int colStart;
    private int colEnd;
    private int rowStart;
    private int rowEnd;

    public SpirallingArray(int[][] array) {
        this.array = array;
        this.rowHigh = 0;
        this.rowLow = array.length - 1;
        this.colLeft = 0;
        this.colRight = array[0].length - 1;
        this.colStart = 0;
        this.colEnd = array[0].length;
        this.rowStart = 1;
        this.rowEnd = array.length;
    }


    void printSpiralOrder() {
        int count = array.length * array[0].length;
        while (count > 0) {
            count = printHigh(count);
            count = printRight(count);
            count = printLow(count);
            count = printLeft(count);
        }
    }

    private int printLeft(int count) {
        for (int i = rowEnd - 1; i > rowStart - 1; i--) {
            if (count == 0){
                System.out.print(array[i][colLeft] + ".");
            }else {
                System.out.print(array[i][colLeft] + ",");
            }
            count--;
        }
        colLeft++;
        rowStart++;
        return count;
    }

    private int printLow(int count) {
        for (int i = colEnd - 1; i > colStart - 1; i--) {
            if (count == 1){
                System.out.print(array[rowLow][i] + ".");
            }else {
                System.out.print(array[rowLow][i] + ",");
            }
                count--;

        }
        rowLow--;
        colStart++;
        return count;
    }

    private int printRight(int count) {
        for (int i = rowStart; i < rowEnd; i++) {
            if (count == 1){
                System.out.print(array[i][colRight] + ".");
            }else {
                System.out.print(array[i][colRight] + ",");
            }
                count--;

        }
        colRight--;
        rowEnd--;
        return count;
    }

    private int printHigh(int count) {
        for (int i = colStart; i < colEnd; i++) {
            if (count == 1){
                System.out.print(array[rowHigh][i] + ".");
            }else {
                System.out.print(array[rowHigh][i] + ",");
            }
            count--;
        }
        rowHigh++;
        colEnd--;
        return count;
    }
}
