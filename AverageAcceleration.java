package algorithms;

public class AverageAcceleration {
    int v0;
    int v1;
    int span;

    public AverageAcceleration(int v0, int v1, int span) {
        this.v0 = v0;
        this.v1 = v1;
        this.span = span;
    }

    void displayAverageAcceleration() {
        System.out.println((v1 - v0) / span);
    }
}
