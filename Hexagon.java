package algorithms;

public class Hexagon {
    private int a;

    public Hexagon(int a) {
        this.a = a;
    }

    double area() {
        return 3 * a * a * Math.sqrt(3.0) / 2;
    }

    int perimeter() {
        return  6 * a;
    }

    double shortDiagonal() {
        return a * Math.sqrt(3.0);
    }
    int longDiagonal() {
        return 2 * a;
    }
    double radiusInscribedCircle() {
        return shortDiagonal() / 2;
    }
}
