package algorithms;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SudokuVerifier {
    boolean isSudokuValid(int[][] board) {
        Set<Integer> check = new HashSet<>();
        List<Integer> empty = new ArrayList<>();
        empty.add(10);
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (!check.add(board[i][j])) {
                    return false;
                }
            }
            check.retainAll(empty);
        }
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (!check.add(board[j][i])) {
                    return false;
                }
            }
            check.retainAll(empty);
        }
        for (int i = 0; i < 9; i += 3) {
            for (int j = 0; j < 9; j += 3) {
                for (int k = i; k < i + 3; k++) {
                    for (int l = j; l < j + 3; l++) {
                        if (!check.add(board[k][l])) {
                            return false;
                        }
                    }
                }
                check.retainAll(empty);
            }
        }
        return true;
    }
}
