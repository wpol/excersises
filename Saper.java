package algorithms;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

public class Saper {
    private int n;
    private int m;
    private int p;
    private boolean[][] boolTable;

    public Saper(int n, int m, int p) {
        this.n = n;
        this.m = m;
        this.p = p;
    }

    public void displaySolution() {
        char[][] solutionTable;
        createBoolTable();
        solutionTable = createCharTable(boolTable);
        printSolutionTable(solutionTable);
    }

    private void createBoolTable() {
        Random random = new Random();
        boolTable = new boolean[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int number = random.nextInt(100);
                if (number <= p - 1) {
                    boolTable[i][j] = true;
                }
            }
        }
    }

    private char[][] createCharTable(boolean[][] matrix) {
        char[][] numbers = new char[matrix.length][matrix[0].length];

        int xStart = 0;
        int yStart = 0;
        int xEnd = matrix.length - 1;
        int yEnd = matrix[0].length - 1;

        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers[0].length; j++) {
                char number;
                if (i == xStart && j == yStart) {
                    number = Character.forDigit((int) Stream.of(matrix[i][j + 1], matrix[i + 1][j]
                            , matrix[i + 1][j + 1]).filter(b -> b).count(), 10);
                } else if (i == xStart && j == yEnd) {
                    number = Character.forDigit((int) Stream.of(matrix[i][j - 1], matrix[i + 1][j]
                            , matrix[i + 1][j - 1]).filter(b -> b).count(), 10);
                } else if (i == xEnd && j == yStart) {
                    number = Character.forDigit((int) Stream.of(matrix[i][j + 1], matrix[i - 1][j]
                            , matrix[i - 1][j + 1]).filter(b -> b).count(), 10);
                } else if (i == xEnd && j == yEnd) {
                    number = Character.forDigit((int) Stream.of(matrix[i][j - 1], matrix[i - 1][j]
                            , matrix[i - 1][j - 1]).filter(b -> b).count(), 10);
                } else if (i == xStart) {
                    number = Character.forDigit((int) Stream.of(matrix[i][j - 1], matrix[i][j + 1]
                            , matrix[i + 1][j - 1], matrix[i + 1][j], matrix[i + 1][j + 1])
                            .filter(b -> b).count(), 10);
                } else if (i == xEnd) {
                    number = Character.forDigit((int) Stream.of(matrix[i][j - 1], matrix[i][j + 1]
                            , matrix[i - 1][j - 1], matrix[i - 1][j], matrix[i - 1][j + 1])
                            .filter(b -> b).count(), 10);
                } else if (j == yStart) {
                    number = Character.forDigit((int) Stream.of(matrix[i - 1][j], matrix[i + 1][j]
                            , matrix[i - 1][j + 1], matrix[i][j + 1], matrix[i + 1][j + 1])
                            .filter(b -> b).count(), 10);
                } else if (j == yEnd) {
                    number = Character.forDigit((int) Stream.of(matrix[i - 1][j], matrix[i + 1][j]
                            , matrix[i - 1][j - 1], matrix[i][j - 1], matrix[i + 1][j - 1])
                            .filter(b -> b).count(), 10);
                } else {
                    number = Character.forDigit((int) Stream.of(matrix[i - 1][j - 1], matrix[i - 1][j]
                            , matrix[i - 1][j + 1], matrix[i + 1][j - 1], matrix[i + 1][j], matrix[i + 1][j + 1]
                            , matrix[i][j - 1], matrix[i][j + 1]).filter(b -> b).count(), 10);
                }
                numbers[i][j] = number;
            }
        }
        return numbers;
    }

    private void printSolutionTable(char[][] solutionTable) {
        for (int i = 0; i < n; i++) {
            printLine();
            System.out.println();
            System.out.print("|");
            for (int j = 0; j < m; j++) {
                if (boolTable[i][j]) {
                    System.out.print(" * |");
                } else System.out.print(" " + solutionTable[i][j] + " |");
            }
            System.out.println();
        }
        printLine();
    }

    private void printLine() {
        for (int i = 0; i < m * 4 + 1; i++) {
            System.out.print("-");
        }
    }
}
