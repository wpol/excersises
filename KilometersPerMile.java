package algorithms;

public class KilometersPerMile {
    private static final double KILOMETER_PER_MILE = 1609;

    void kilometers() {
        double miles = 100;
        double kilometers = miles * KILOMETER_PER_MILE;
        System.out.println(kilometers);
    }
}
