package algorithms;

import java.awt.*;
import java.util.Random;

public class RandomWalker {
    private int n;
    private boolean[][] board;
    private Point start;
    private int xMove;
    private int yMove;
    private Random generator = new Random();
    private int count = 0;
    private int countTrue = 0;

    public RandomWalker(int n) {
        this.n = n;
    }

    public int goAndCount() {
        createBoard();
        setLocation();
        while (countTrue < n * n) {
            moveAndMarkTrueIfFalse();
            count++;
        }
        return count;
    }

    private void createBoard() {
        board = new boolean[n][n];

    }

    private Point setLocation() {
        start = new Point();
        start.setLocation(generator.nextInt(n), generator.nextInt(n));
        return start;
    }

    private void moveAndMarkTrueIfFalse() {
        do {
            drawLotsMove();
        }
        while (((int) start.getX() + xMove) < 0 || ((int) start.getX() + xMove) > n - 1
                || ((int) start.getY() + yMove) < 0 || (((int) start.getY() + yMove)) > n - 1);
        if (!board[(int) start.getX() + xMove][(int) start.getY() + yMove]) {
            board[(int) start.getX() + xMove][(int) start.getY() + yMove] = true;
            countTrue++;
        }
        start.move((int) start.getX() + xMove, (int) start.getY() + yMove);
    }

    private void drawLotsMove() {
        int draw = generator.nextInt(4);
        switch (draw) {
            case 0:
                xMove = 0;
                yMove = 1;
                break;
            case 1:
                xMove = 0;
                yMove = -1;
                break;
            case 2:
                xMove = 1;
                yMove = 0;
                break;
            case 3:
                xMove = -1;
                yMove = 0;
                break;
        }
    }

}
