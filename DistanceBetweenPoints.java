package algorithms;

import java.awt.*;

public class DistanceBetweenPoints {
    private Point point1;
    private Point point2;

    public DistanceBetweenPoints(Point point1, Point point2) {
        this.point1 = point1;
        this.point2 = point2;
    }

    void displayDistance() {
        System.out.printf("%.2f \n",(Math.sqrt((point1.getX() - point2.getX()) * (point1.getX() - point2.getX())
                +  (point1.getY() - point2.getY()) * (point1.getY() - point2.getY()))));
    }
}
