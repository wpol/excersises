package algorithms;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        AmericanFlag americanFlag = new AmericanFlag();
        americanFlag.printAmerianFlag();

        Hexagon hexagon = new Hexagon(5);
        System.out.println(hexagon.area());
        System.out.println(hexagon.longDiagonal());
        System.out.println(hexagon.shortDiagonal());
        System.out.println(hexagon.perimeter());
        System.out.println(hexagon.radiusInscribedCircle());

        CountSpaces countSpaces = new CountSpaces();
        countSpaces.printNumberOfSpaces("quater past seven");


        IntegerSum integerSum = new IntegerSum(15);
        System.out.println(integerSum.sum());

        DivisibleNumbers divisibleNumbers = new DivisibleNumbers();
        divisibleNumbers.numbersDivisibleByThreeAndFive();

        HoursMinuteSecond hoursMinuteSecond = new HoursMinuteSecond(86399);
        hoursMinuteSecond.printHour();

        TwoDimension twoDimension = new TwoDimension(9);
        twoDimension.createAndPrintArray();

        int[][] board = {{5, 3, 4, 6, 7, 8, 9, 1, 2},
                {6, 7, 2, 1, 9, 5, 3, 4, 8},
                {1, 9, 8, 3, 4, 2, 5, 6, 7},
                {8, 5, 9, 7, 6, 1, 4, 2, 3},
                {4, 2, 6, 8, 5, 3, 7, 9, 1},
                {7, 1, 3, 9, 2, 4, 8, 5, 6},
                {9, 6, 1, 5, 3, 7, 2, 8, 4},
                {2, 8, 7, 4, 1, 9, 6, 3, 5},
                {3, 4, 5, 2, 8, 6, 1, 7, 9}};
        SudokuVerifier sudokuVerifier = new SudokuVerifier();
        System.out.println(sudokuVerifier.isSudokuValid(board));

        Saper saper = new Saper(4,6,35);
        saper.displaySolution();

        RandomWalker randomWalker = new RandomWalker(100);
        System.out.println(randomWalker.goAndCount());

        AverageAcceleration averageAcceleration = new AverageAcceleration(10, 25, 3);
        averageAcceleration.displayAverageAcceleration();

        HeatWater heatWater = new HeatWater(1.5, 20.0, 100.0);
        System.out.println(heatWater.calculateEnergy());

        DistanceBetweenPoints distanceBetweenPoints = new DistanceBetweenPoints(new Point(2, 3), new Point(4, 2));
        distanceBetweenPoints.displayDistance();

        Lottery lottery = new Lottery(15);
        lottery.drawLots();

        ChineseZodiac chineseZodiac = new ChineseZodiac(1888);
        chineseZodiac.displayChineseZodiacSign();

        int[][] array = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
        SpirallingArray spirallingArray = new SpirallingArray(array);
        spirallingArray.printSpiralOrder();

        DayOfWeek dayOfWeek = new DayOfWeek(2019, 02, 20);
        dayOfWeek.displayDayOfWeek();

        ZipCode zipCode = new ZipCode("08540");
        zipCode.printBarCode();

        Lockers lockers = new Lockers();
        System.out.println(lockers.toggleAndCount());


    }
}
