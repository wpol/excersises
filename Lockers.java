package algorithms;

public class Lockers {
    private boolean[] lockers = new boolean[100];

    public int toggleAndCount() {
        int count = 0;
        for (int i = 2; i < lockers.length + 1; i++) {
            for (int j = 0; j < lockers.length; j++) {
                if ((j + 1) % i == 0) {
                    toggle(j);
                }
            }
        }
        for (int i = 0; i < lockers.length; i++) {
            if (!lockers[i]) count++;
        }
        return count;
    }

    private void toggle(int i) {
        lockers[i] = !lockers[i];
    }
}
